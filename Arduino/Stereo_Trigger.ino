/*
 Chat Server

 A simple server that distributes any incoming messages to all
 connected clients.  To use, telnet to your device's IP address and type.
 You can see the client's input in the serial monitor as well.
 Using an Arduino Wiznet Ethernet shield.

 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13

 created 18 Dec 2009
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe

 */

#include <SPI.h>
#include <Ethernet.h>

String inString = "";
int freq = 1; 
int freqOld = 1; 
int ledPin1 = 9; 
int ledPin2 = 5; 
int ledPinCheck = 7;
unsigned long previousMillis;

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network.
// gateway and subnet are optional:
byte mac[] = {
  0x90, 0xA2, 0xDA, 0x10, 0x25, 0x04
};
IPAddress ip(192, 168, 0, 10);
IPAddress myDns(192,168,0, 10);
IPAddress gateway(192, 168, 0, 10);
IPAddress subnet(255, 255, 0, 0);

// telnet defaults to port 23
EthernetServer server(23);
boolean alreadyConnected = false; // whether or not the client was connected previously

void setup() {
  // set the digital pin as output 
  pinMode(ledPin1, OUTPUT); 
  digitalWrite(ledPin1,LOW);
  pinMode(ledPin2, OUTPUT); 
  digitalWrite(ledPin2,LOW);
  pinMode(ledPinCheck, OUTPUT); 
  digitalWrite(ledPinCheck,LOW);
  
  // initialize the ethernet device
  //Ethernet.begin(mac, ip, myDns, gateway, subnet);
  Ethernet.begin(mac, ip);

  // start listening for clients
  server.begin();
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  previousMillis = millis();
  
  Serial.print("Chat server address:");
  Serial.println(Ethernet.localIP());
}

void loop() {
  // wait for a new client:
  EthernetClient client = server.available();

  // when the client sends the first byte, say hello:
  if (client) {
    if (!alreadyConnected) {
      // clear out the input buffer:
      client.flush();
      Serial.println("We have a new client");
      client.println("Hello, client!");
      alreadyConnected = true;
    }

    if (client.available() > 0) {
      // read the bytes incoming from the client:
      char thisChar = client.read();
      // echo the bytes back to the client:
      //server.write(thisChar);
      if (isDigit(thisChar)){
        inString += thisChar; 
      }
      if (thisChar == '\n'){
        freq = inString.toInt(); 
        if (freq > 0 && freq < 1000){
          Serial.print("\n Frequency:"); 
          Serial.println(freq);
          server.write("new Freq. (Hz)");
          String freqStr = String(freq);
          server.println(freqStr);
          server.print("Enter freq btw 0 to 1000 Hz: ");
          inString = "";
          freqOld = freq;
          
        }
        else{
          server.println("Not a valid freq. btw 0 and 1000 Hz");
          server.print("Enter freq btw 0 to 1000 Hz: ");
          freq = freqOld;
        }
      }
      // echo the bytes to the server as well:
      Serial.write(thisChar);
      

    }
  }
  
      //flash LED to frequency
      unsigned long period = (1.0/freq)*1000;
      unsigned long delayPeriod = 10.0; //ms
      if ((previousMillis+period)<= millis()){
        digitalWrite(ledPin1,HIGH);
        digitalWrite(ledPin2,HIGH);
        digitalWrite(ledPinCheck,HIGH);
        //digitalWrite(ledPinCheck,LOW); 
        Serial.println(period);
        previousMillis = millis(); 
      }
      if ((previousMillis+delayPeriod)<= millis()){
        digitalWrite(ledPin1,LOW);
        digitalWrite(ledPin2,LOW);
        digitalWrite(ledPinCheck,LOW); 
      }
      
}
