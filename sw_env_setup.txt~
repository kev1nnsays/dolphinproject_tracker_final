Required packages:
-ROS
-ROS Packages: prosilica_gige_sdk, prosilica_driver
-Opencv 3.0
-Opencv contrib
-VimbaViewer
-Python



###Setting up openCV3 with nonfree modules###
- Make a source folder
mkdir ~/opencv_source

- Download newest version of OpenCV to source folder

- To get modules git clone the following into the source folder:
https://github.com/Itseez/opencv_contrib

-Go into the OpenCV folder and make a directory called build
cd ~/opencv_source/OpenCV/build

-Execute the following: 
cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=/usr/local \
	-D INSTALL_C_EXAMPLES=OFF \
	-D INSTALL_PYTHON_EXAMPLES=ON \
	-D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules \
	-D BUILD_EXAMPLES=ON ..

-Compile Opencv in the build dir with 4 parallel processes:
make -j4
sudo make install
sudo ldconfig

-Now openCV should work with nonfree modules. Check with the following
python
>>!#/usr/bin/env python
>>import cv2
>>help(cv2.xfeatures2d)




###Setting Camera IP's###
Each camera has their own IP, which can be automatically generated or persistent (static). 
The default is "automatic" so we need to change to "persistent"
- In your networking, open up "Edit Connections"
- Make a connection with IPv4 Settings with "Local-link only"; allows for 
auto-detection of camera's IP
- Open VimbaViewer: $ ~./Vimba_1_4/Tools/Viewer/Bin/x86_64bit/VimbaViewer
- You should see the camera listed
- To view image press the play button
- To change IP, right click on the camera and edit
- Change to desired IP and change to persistent



###ROS Setup###
- Install ROS according to website http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment

- If ROS environmental variables aren't already added at start of bash.rc, use source /opt/ros/<distro>/setup.bash; otherwise you can add using:
echo "source /opt/ros/jade/setup.bash" >> ~/.bashrc
source ~/.bashrc

###Making a ROS Workspace, where code is hosted###
- Start a terminal

- set up workspace: $ mkdir -p ~/<catkin_ws_name>/src

- move into source directory: $ cd src
- Make a catkin workspace: $ catkin_make_workspace

- place needed packages in src such as prosilica_driver and prosilica_gige_sdk

- return to root workspace directory: $ cd ~/<catkin_ws_name>

- build the source code: $ catkin_make

###Viewing images###
- source environmental variables: $ source ./devel/setup.bash

- new terminal and start roscore: $ roscore

- new terminal, initialize a rosnode to publish images 
  from Proscilica camera: 
	$ cd ~/<catkin_ws_name>/src/prosilica_driver/prosilica_camera/
	$ roslaunch streaming.launch 
	*** (make sure the "ip_address" inside streaming.launch is same as camera's IP)
- check that images are being published: $ rostopic list
	should see a topic "/image_raw"

- to check if images are correct: $rosrun image_view image_view image:=/image_raw

- new terminal, run the python script for converting ros messages into openCV matrices:
	$ cd ~/<catkin_ws_name>/src/
	$ ./ros2opencvTest.py (should be executable; chmod +x ros2opencvTest.py)
	a window with streaming video should pop up


